package ru.t1.nikitushkina.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.nikitushkina.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.nikitushkina.tm.enumerated.Sort;

import java.util.List;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IUserOwnedDTORepository<M> {

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    void removeById(@Nullable String userId, @Nullable String id);

}
