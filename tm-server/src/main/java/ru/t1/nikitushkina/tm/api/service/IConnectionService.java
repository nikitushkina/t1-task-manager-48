package ru.t1.nikitushkina.tm.api.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface IConnectionService {

    @NotNull
    EntityManager getEntityManager();

    @NotNull
    Liquibase getLiquibase();

    void close();

}
