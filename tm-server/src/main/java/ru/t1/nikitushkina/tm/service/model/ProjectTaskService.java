package ru.t1.nikitushkina.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.service.model.IProjectService;
import ru.t1.nikitushkina.tm.api.service.model.IProjectTaskService;
import ru.t1.nikitushkina.tm.api.service.model.ITaskService;
import ru.t1.nikitushkina.tm.exception.entity.ProjectNotFoundException;
import ru.t1.nikitushkina.tm.exception.entity.TaskNotFoundException;
import ru.t1.nikitushkina.tm.exception.field.ProjectIdEmptyException;
import ru.t1.nikitushkina.tm.exception.field.TaskIdEmptyException;
import ru.t1.nikitushkina.tm.exception.user.UserIdEmptyException;
import ru.t1.nikitushkina.tm.model.Task;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    public ProjectTaskService(
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(projectService.findOneById(projectId));
        taskService.updateProjectIdById(userId, taskId, projectId);
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        projectService.removeById(userId, projectId);
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(null);
        taskService.updateProjectIdById(userId, taskId, null);
    }

}
