package ru.t1.nikitushkina.tm.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.junit.Test;

public class UserSchemeTest extends AbstractSchemeTest {

    @Test
    public void Test() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("user");
    }

}
