package ru.t1.nikitushkina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.model.IWBS;
import ru.t1.nikitushkina.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
public class Task extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUId = 1;

    @NotNull
    @Column(length = 150)
    private String name = "";

    @NotNull
    @Column(length = 250)
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @Column
    @Nullable
    private Date created = new Date();

    public Task(@NotNull final String name) {
        this.name = name;
    }

    public Task(
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
